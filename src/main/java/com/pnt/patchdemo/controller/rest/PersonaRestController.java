package com.pnt.patchdemo.controller.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.pnt.patchdemo.domain.Persona;
import com.pnt.patchdemo.repository.PersonaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonaRestController {

    private final PersonaRepository personaRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public PersonaRestController(PersonaRepository personaRepository) {
        this.personaRepository = personaRepository;
    }

    @GetMapping(path = "persona/{id}")
    public ResponseEntity<Persona> get(@PathVariable Long id) {
        Persona persona = personaRepository.findById(id);
        return ResponseEntity.ok(persona);

    }

    @PatchMapping(path = "persona/{id}", consumes = "application/json")
    public Persona updatePersona(@PathVariable Long id, @RequestBody JsonPatch patch) throws JsonPatchException, JsonProcessingException {        
        Persona personaTarget = personaRepository.findById(id);
        Persona personaPatched = applyPatchToCustomer(patch, personaTarget);
        return personaPatched;

    }

    private Persona applyPatchToCustomer(JsonPatch patch, Persona personaTarget) throws JsonPatchException, JsonProcessingException {
        JsonNode patched = patch.apply(objectMapper.convertValue(personaTarget, JsonNode.class));
        return objectMapper.treeToValue(patched, Persona.class);
    }
}
