package com.pnt.patchdemo.repository;

import com.pnt.patchdemo.domain.Persona;
import java.util.Arrays;
import org.springframework.stereotype.Repository;

@Repository
public class PersonaRepository {

    public Persona findById(Long id) {
        Persona persona = new Persona();
        persona.setId(1L);
        persona.setNombre("Nombre");
        persona.setApellido("apellido");
        persona.setTelefonos(Arrays.asList("tel1"));
        return persona;
    }
}
