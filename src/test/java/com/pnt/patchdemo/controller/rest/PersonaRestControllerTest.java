package com.pnt.patchdemo.controller.rest;

import com.pnt.patchdemo.PatchDemoApplicationTests;
import io.restassured.http.ContentType;
import io.restassured.internal.ResponseSpecificationImpl;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.Test;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.*;
import static org.hamcrest.Matchers.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.WebApplicationContext;

public class PersonaRestControllerTest extends PatchDemoApplicationTests {

    @Autowired
    private WebApplicationContext context;

    @BeforeEach
    public void before() {
        RestAssuredMockMvc.webAppContextSetup(context);
    }

    @AfterEach
    public void after() {
        RestAssuredMockMvc.reset();
    }

    @Test
    public void getPersonaje_existe_retornaPersonaje() {
        when()
                .get("/persona/1")
                .then()
                .status(HttpStatus.OK)
                .body("id", equalTo(1));
    }

    @Test
    public void pathPersona_cambiaNombre_nombreCambiado() {
        given()
                .contentType(ContentType.JSON)
                .body("[{"
                        + "    \"op\":\"replace\","
                        + "    \"path\":\"/nombre\","
                        + "    \"value\":\"este es otro nombre\""
                        + "}]")
                .when().patch("/persona/1")
                .then().body("nombre", equalTo("este es otro nombre"));
    }

    @Test
    public void pathPersona_agregaTelefono_telefonoAgregado() {
        given()
                .contentType(ContentType.JSON)
                .body("[{"
                        + "    \"op\":\"add\","
                        + "    \"path\":\"/telefonos/0\","
                        + "    \"value\":\"tel0\""
                        + "}]")
                .when().patch("/persona/1")
                .then().body("telefonos", contains("tel0", "tel1"));
    }

}
